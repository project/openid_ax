<?php

/**
 * @file
 * Page callbacks for OpenID AX.
 */

/**
 * Primary endpoint callback - serves op_endpoint.
 *
 * @param array $request (Optional) request parameters.
 */
function openid_ax_endpoint($request = array()) {
  module_load_include('inc', 'openid');
  module_load_include('inc', 'openid_ax');
  if (count($request) == 0) {
    $request = _openid_response();
  }
  $ax_request = openid_ax_process($request);
  return $ax_request;
}

/**
 * Menu callback to continue authentication process after user login. This
 * callback is encountered when a user tries to login to an RP but does not yet
 * have a valid local session
 */
function openid_ax_continue() {
  if (isset($_SESSION['openid_ax']['request'])) {
    $request_ax = $_SESSION['openid_ax']['request'];
    unset($_SESSION['openid_ax']['request']);
    return openid_ax_endpoint($request_ax);
  }
  else {
    drupal_set_message(t('Session expired'));
    drupal_goto();
  }
}

/**
 * Form for user interaction before sending AX values to RP
 */
function openid_ax_form(&$form_state,&$ax_response) {
  global $user;
  $form_state['ax_response'] = $ax_response;
  $realm = $form_state['post']['openid_realm'];
  if($realm == '') {
  	$realm = $_SESSION['openid_ax']['realm'];
  	unset($_SESSION['openid_ax']['realm']);
  }
  $form = array();
  $form['intro'] = array(
    '#type' => 'markup',
    '#value' => '<p>'. t('Your following details are being send to %site, would you like to continue?', array('%site' => $realm)) . '</p>'
  );
  foreach($ax_response as $name=>$value) {
  	if(strstr($name,'value')){
	  $form[$name] = array(
	    '#type' => 'textfield',
	    '#title' => t(substr(strrchr($name,'value.'),6)),
	    '#value' => $value,
	    '#size' => 14,
	    '#maxlength' => 100
	  );
    }
    else {
      $form[$name] = array(
	    '#type' => 'hidden',
	    '#title' => $name,
	    '#value' => $value
	  );
    }
  }
  $form['#action'] = url('openid/ax/send');
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Yes; Send my data'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('No! Don\'t send my data'),
    '#submit' => array('openid_ax_form_submit_cancel')
  );
  return $form;
}

/**
 * Submit handler for openid_ax_form through page call back for openid_ax_send
 */
function openid_ax_send() {
  module_load_include('inc', 'openid');
  module_load_include('inc', 'openid_ax');
  $response = _openid_response();
  $ax_response = openid_ax_fetch_response($response);
  drupal_goto($_SESSION['openid_ax']['return_to'],$ax_response);
}

/**
 * Menu callback for openid_ax_persona
 */
function openid_ax_persona() {
  return drupal_get_form('openid_ax_persona_form');
}

/**
 * Form for creating/editing a persona
 */
function openid_ax_persona_form(&$form_state) {
  global $user;
  $form = array();
  if(isset($form_state['storage']['personas'])) {
  	$personas = $form_state['storage']['personas'];
  }
  elseif(isset($_GET['persona'])) {
  	$personas = $_GET['persona'];
  }   
  else {
  	$personas = 0;
  }
  $persona = db_query("SELECT * FROM {openid_ax_persona} where uid=%d", $user->uid);
  $options = array(t('Default'), 'openid_ax_create_new' =>t('Create new'), 'manage_ax_personas' => t('Manage AX Personas'));
  while($persona_value = db_fetch_array($persona)) {
  	$options[$persona_value['persona_id']] = $persona_value['persona_name']; 
  }
  if(count($options) == 3) {
  	unset($options['manage_ax_personas']);
  }
  $form['personas'] = array(
    '#type' => 'select',
    '#title' => 'Personas',
    '#options' => $options,
    '#default_value' => $personas
  );
  $form['select'] = array(
    '#type' => 'submit',
    '#value' => 'Select Persona',
    '#submit' => array('openid_ax_persona_form_submit_select_persona')
    );
  $identifiers = db_query("SELECT * FROM {openid_ax_attributes}");
  while($id = db_fetch_array($identifiers)) {
  	$identifier[$id['ax_id']] = $id['identifier'];
  }
  $ax_values = db_query("SELECT * FROM {openid_ax_values} WHERE uid=%d and persona_id='%d'",$user->uid, $personas);
  $valueExist = array();
  while($row = db_fetch_array($ax_values)) {
  	$valueExist[] = $row['ax_id'];  
  	$form[$row['vid']] = array(
	  '#type' => 'textfield',
	  '#title' => t($identifier[$row['ax_id']]),
	  '#default_value' => $row['ax_values'],
	  '#size' => 25,
	  '#maxlength' => 100
    );
	$form['has_value'.$row['vid']] = array(
	  '#type' => 'hidden',
	  '#value' => $row['vid'],
	);
  	static $count = 0;
	$count++;
	static $submit_btn = 1;
	if($count == 10) {
	  $form['submit'.$submit_btn] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
      );
      $count = 0;
      $submit_btn++;
	}
	$hasValues = TRUE;
  }
  if($hasValues){ 
	$form['intro'] = array(
	  '#type' => 'markup',
	  '#value' => t('<strong>To delete a value, delete the value in the particular field and then submit</strong>'),
	  '#weight' => -1
    );
  }
  foreach($identifier as $key => $value) {
  	if(!(in_array($key, $valueExist))){
  	  $form['vac'.$key] = array(
	    '#type' => 'textfield',
	    '#title' => t($value),
	    '#default_value' => '',
	    '#size' => 25,
	    '#maxlength' => 100
      );
      $form['no_value'.$key] = array(
	  '#type' => 'hidden',
	  '#value' => $key,
	);
      static $count = 0;
	  $count++;
	  static $submit_btn = 1;
	  if($count == 10) {
	    $form['submit'.$submit_btn] = array(
          '#type' => 'submit',
          '#value' => t('Submit'),
        );
        $count = 0;
        $submit_btn++;
	  }
  	}
  } 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Select submit handler for openid_ax_form
 */
function openid_ax_persona_form_submit_select_persona(&$form, &$form_state) {
  global $user;
  if($form_state['values']['personas']=='openid_ax_create_new') {
	drupal_goto('user/'.$user->uid.'/persona/create');
  }
  elseif($form_state['values']['personas']=='manage_ax_personas') {
  	drupal_goto('user/'.$user->uid.'/persona/manage');
  }
  else {
  	$form_state['storage']['personas'] = $form_state['values']['personas'];
  }
}

/**
 * Submit handler for openid_ax_persona form
 */
function openid_ax_persona_form_submit(&$form, &$form_state) {
  global $user;
  $result = db_query("SELECT vid FROM {openid_ax_values} WHERE uid='%d' and persona_id='%d'",$user->uid, $form_state['values']['personas']);
  static $success;
  $success = FALSE;
  while($vid = db_fetch_array($result)) {
    $ax_value = trim($form_state['values'][$vid['vid']]);
    if(isset($form_state['values'][has_value.$vid['vid']])){
      if($ax_value != ''){
	    $success = db_query("UPDATE {openid_ax_values} SET ax_values='%s' WHERE vid='%d'", $form_state['values'][$vid['vid']], $vid['vid']);	
	  }
	  else {
	    $success = db_query("DELETE FROM {openid_ax_values} WHERE vid='%d'", $vid['vid']);
	  }
  	}
  }
  $ax_ids = db_query("SELECT ax_id FROM {openid_ax_attributes}");
  while($ax_id=db_fetch_array($ax_ids)) {
  	if((isset($form_state['values'][no_value.$ax_id['ax_id']]))&&($form_state['values'][vac.$ax_id['ax_id']] != '')){
  	  $success = db_query("INSERT INTO {openid_ax_values}(ax_id, uid, persona_id, ax_values) VALUES ('%d', '%d', '%d', '%s')",$ax_id['ax_id'], $user->uid, $form_state['values']['personas'], $form_state['values'][$ax_id['ax_id']]);
  	} 
  }
  if($success) {
  	drupal_set_message(t('Your persona values have been saved'));
  }
}

/**
 * Page callback for creating new persona
 */
function openid_ax_create_persona() {
  return drupal_get_form('openid_ax_new_persona_form');  
}

/**
 * Form for creating new persona
 */
function openid_ax_new_persona_form() {
  $form = array();
  $form['persona'] = array(
	'#type' =>'textfield',
	'#title' => t('New Persona name'),
	'#default_value' => '',
	'#required' => TRUE,
	'#size' => 20
	);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit handler for openid_ax_new_persona
 */
function openid_ax_new_persona_form_submit(&$form, $form_state) {
  global $user;
  $persona_exists = db_result(db_query("SELECT * FROM {openid_ax_persona} WHERE uid=%d AND persona_name='%s'", $user->uid, $form_state['values']['persona']));
  if($persona_exists) {
  	drupal_set_message(t('A persona with that name already exists. Please select another persona name.'),'error');
  }
  else {
  	$max_persona_id = db_result(db_query("SELECT MAX(persona_id) FROM {openid_ax_persona} WHERE uid='%d'", $user->uid));
  	$create_persona = db_query("INSERT INTO {openid_ax_persona} (uid, persona_id, persona_name) VALUES ('%d', '%d', '%s')", $user->uid, $max_persona_id+1, $form_state['values']['persona']);
  	if($create_persona) {
  	  drupal_set_message(t('New Persona created'));
  	  drupal_goto('user/'.$user->uid.'/persona');
  	}
  }
} 

/**
 * Page to manage AX personas, viz, rename, delete, copy, etc.
 */
function openid_ax_manage_personas() {
  global $user;
  $profiles = (db_query("SELECT * FROM {openid_ax_persona} WHERE uid=%d",$user->uid));
  $header = array (
    'Persona Name',
    'Edit Persona Values',
    'Rename Persona',
    'Delete Persona',
  );
  $rows['default'] = array (
  'Default',
  '<a href="edit?persona=0">Edit</a>',
  '-N/A-',
  '-N/A-'
  );
  while($profile = db_fetch_array($profiles)) {
  	$rows[$profile['persona_name']] = array (
  	  $profile['persona_name'],
  	  '<a href="edit?persona='.$profile['persona_id'].'">Edit </a>',
  	  '<a href="rename?p='.$profile['persona_id'].'">Rename</a>',
  	  '<a href="delete?p='.$profile['persona_id'].'">Delete</a>',
  	);
  }
  $content = theme_table($header, $rows);
  $content .= drupal_get_form('copy_persona_values_form');
  return $content;
}

/**
 * Form for selecting from to persona for values to be copied 
 */
function copy_persona_values_form() {
  global $user;
  $form = array();
  $persona = db_query("SELECT * FROM {openid_ax_persona} WHERE uid='%d'",$user->uid);
  $options = array(t('Default'));
  while($persona_value = db_fetch_array($persona)) {
  	$options[$persona_value['persona_id']] = $persona_value['persona_name']; 
  }
  $form['intro'] = array(
    '#type' => 'markup',
    '#value' => t('<b>Copy values from one persona into another:</b>'),
  );
  $form['from_personas'] = array(
    '#type' => 'select',
    '#title' => 'From',
    '#options' => $options,
  );
  $form['to_personas'] = array(
    '#type' => 'select',
    '#title' => 'To',
    '#options' => $options,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Copy'),
  );
  return $form;
}

/**
 * copy_persona_values form submit handler
 */
function copy_persona_values_form_submit(&$form, $form_state) {
  global $user;
  $target_persona = $form_state['values']['to_personas'];
  $copySrc = db_query("SELECT ax_id,ax_values FROM {openid_ax_values} WHERE uid='%d' AND persona_id='%d'",$user->uid, $form_state['values']['from_personas']);
  while($vals = db_fetch_array($copySrc)) {
    $copyTrg = db_query("INSERT INTO {openid_ax_values}(ax_id, uid, persona_id, ax_values)  VALUES ('%d','%d','%d','%s')", $vals['ax_id'], $target_persona, $user->uid, $vals['ax_values']);
  }
  drupal_set_message(t('Persona values copied.'));
}

/**
 * Page call back for renaming persona
 */
function openid_ax_rename_personas() {
  $content = drupal_get_form('openid_ax_rename_persona_form', $_GET['p']);
  $content .= openid_ax_manage_personas();
  return $content;	
}

/**
 * Form for renaming persona
 */
function openid_ax_rename_persona_form(&$form, $form_state) {
  global $user;
  isset($_GET['p'])?$persona_id=$_GET['p']:$persona_id=$form_state['storage']['persona_id'];
  $form = array();
  $persona = db_result(db_query("SELECT persona_name FROM {openid_ax_persona} WHERE persona_id='%d' AND uid='%d'",$persona_id, $user->uid));
  $form['persona'] = array(
    '#type' => 'textfield',
    '#default_value' => $persona,
    '#size' => 15,
  );
  $form['persona_id'] = array(
    '#type' => 'hidden',
    '#value' => $_GET['p'],
  );
  $form['submit'] = array (
    '#type' => 'submit',
    '#value' => 'Rename',
  );
  return $form;
}

/**
 * Submit handler for openid_ax_rename_persona_form
 */
function openid_ax_rename_persona_form_submit(&$form, &$form_state) {
  global $user;
  $rename = db_query("UPDATE {openid_ax_persona} SET persona_name='%s' WHERE uid='%d' AND persona_id='%d'", $form_state['values']['persona'], $user->uid, $form_state['values']['persona_id']);
  drupal_set_message(t('Your persona has been renamed'));
  $form_state['storage']['persona_id'] = $form_state['values']['persona_id'];
}

/**
 * Function for deleting persona
 */
function openid_ax_persona_delete() {
  global $user;
  $persona_id=$_GET['p'];
  $persona = db_result(db_query("SELECT persona_name FROM {openid_ax_persona} WHERE persona_id='%d' AND uid='%d'",$persona_id, $user->uid));
  $content = t('Are you sure you want to delete your <a href="edit?persona='.$persona_id.'">%profile</a> profile.', array('%profile' => $persona));
  $content .= t(' This cannot be reversed and all your profile related details will be lost.');
  $content .= drupal_get_form('openid_ax_persona_delete_confirm_form');
  return $content;
}

/**
 * Form for confirming if the user wants to delete the persona
 */
function openid_ax_persona_delete_confirm_form() {
  $form = array ();
  $form['persona_id'] = array(
    '#type' => 'hidden',
    '#value' => $_GET['p'],
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] =array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array ('openid_ax_persona_delete_confirm_form_cancel'),
  );
  return $form;
}

/**
 * Submit handler for openid_ax_persona_delete_confirm_form
 */
function openid_ax_persona_delete_confirm_form_submit($form, &$form_state) {
  global $user;
  $delValues = db_query("DELETE FROM {openid_ax_values} WHERE uid='%d'  AND persona_id='%d'", $user->uid, $form_state['values']['persona_id']);
  $deletePersona = db_query("DELETE FROM {openid_ax_persona} WHERE uid='%d'  AND persona_id='%d'", $user->uid, $form_state['values']['persona_id']);
  if($delValues && $deletePersona) {
  	drupal_set_message(t('Persona deleted.'));
  	drupal_goto('user/'.$user->uid.'/persona/manage');
  }
}

/**
 * Cancel handler for openid_ax_persona_delete_confirm_form
 */
function openid_ax_persona_delete_confirm_form_cancel() {
  global $user;
  drupal_goto('user/'.$user->uid.'/persona/manage');
}